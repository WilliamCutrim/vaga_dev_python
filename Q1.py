class Time:
    def __init__(self, n_partidas, gols_pros=0, gols_contras=0):
        self.gols_pros = gols_pros
        self.gols_contras = gols_contras
        self.n_partidas = n_partidas

    def media_gols(self):
        media = (self.gols_pros+self.gols_contras)/self.n_partidas
        
        return media

    def media_pontos_por_partida(self):
        media_pontos = self.status_time()[0]/self.n_partidas
        return media_pontos

    def saldo_gols(self):
        saldo = self.gols_pros
        return saldo

    def status_time(self):

        if self.gols_pros > self.gols_contras:
            pontos = 3
            status = 'Vitoria'

        elif self.gols_pros < self.gols_contras:
            pontos = 0
            status = 'Derrota'
        else:
            pontos = 1
            status = 'Empate'
        return pontos,status

    def __repr__(self):
        partidas = 'Partidas jogadas: %s' % self.n_partidas
        pontos = 'Pontos feitos: %s' % self.status_time()[0]
        saldo = 'Saldo de gols: %s' % self.saldo_gols()
        media = 'Média de gols por partida: %s' % self.media_gols()
        media_pontos = 'Média de pontos por partida: %s' % self.media_pontos_por_partida()


        z = ' \n'.join([partidas,pontos,saldo,media,media_pontos])
        return z

x = Time(n_partidas=2, gols_pros=6, gols_contras=7)
print(x)

