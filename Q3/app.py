from flask import Flask, render_template, request, url_for, redirect
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, template_folder='templates')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///cadastro.sqlite3'

db = SQLAlchemy(app)

class Tabela(db.Model):
    id = db.Column('id',db.Integer, primary_key = True, autoincrement=True)
    nome = db.Column(db.String(150))


    def __init__(self, nome):
        self.nome = nome


@app.route('/')
def index():
    c_nomes = Tabela.query.all()

    return render_template('index.html',c_nomes=c_nomes)

@app.route('/add', methods=['GET', 'POST'])
def add():
    if request.method == 'POST':
        c_nome = Tabela(request.form['nome'])
        db.session.add(c_nome)
        db.session.commit()
        return redirect(url_for('index'))
    return render_template('add.html')

@app.route('/edit/<int:id>', methods=['GET', 'POST'])
def edit(id):
    c_nome = Tabela.query.get(id)
    if request.method == 'POST':
        c_nome.nome =  request.form['nome']
        db.session.commit()
        return redirect(url_for('index'))
    return render_template('edit.html', c_nome=c_nome)


@app.route('/delete/<int:id>')
def delete(id):
    c_nome = Tabela.query.get(id)
    db.session.delete(c_nome)
    db.session.commit()
    return redirect(url_for('index'))


if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)
