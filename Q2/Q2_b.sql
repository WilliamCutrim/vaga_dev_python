SELECT t_usuarios.as id_usuario
,t_usuarios.nome
,t_compras.id as id_compra
,t_compras.valor as valor_compra
,t_compras.item as item_compra

FROM compras as t_compras
FULL OUTER JOIN usuarios as t_usuarios on t_compras.id = t_usuarios.id
ORDER BY id_usuario asc

